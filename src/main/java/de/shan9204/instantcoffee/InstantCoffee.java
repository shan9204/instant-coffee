package de.shan9204.instantcoffee;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

public class InstantCoffee {

    public static void main(String[] args) {
        long duration = 0;
        
        if(args.length > 0) {
            duration = Long.parseLong(args[0]);
        }

        System.out.println("Starting coffee machine...");
        long counter = 0;
        while (true) {
            getSleep();
            coffeeMaker();
            System.out.println("Sipped on coffee number: " + ++counter);
            if (duration != 0 && counter == duration) {
                break;
            }
        }
    }

    private static void getSleep() {
        try {
            Thread.sleep(TimeUnit.MINUTES.toMillis(1));
        } catch (InterruptedException e) {
            System.err.println("Cannot sleep!");
        }
    }

    private static void coffeeMaker() {
        try {
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_F16);
            robot.keyRelease(KeyEvent.VK_F16);
        } catch (AWTException e) {
            System.err.println("Coffee maker does not want to work!");
            e.printStackTrace();
        }
    }
}
